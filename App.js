import React from 'react';
import { View, Alert, Button, StyleSheet, Text, CameraRoll } from 'react-native';
import { MapView } from "expo";


// import * as Permissions from "expo-permissions"; 
// import * as Location from "expo-location"; 
// import * as Camera from "expo-camera"; 




const styles = StyleSheet.create({
  container: {

    flex: 1,
    display: 'flex'

  }

});


export default class App extends React.Component {


  render() {

    return (
      <View style={styles.container}>
        <MapView
          style={{ flex: 1 }}
          initialRegion={
            {
              latitude: -33.460064,
              longitude: -70.652163,
              latitudeDelta: 0.09,
              longitudeDelta: 0.09
            }

          }
        />

      </View>
    );
  }

}